import React from 'react'

import {
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCardImage,
  CCardText,
  CImage,
  CNavLink,
  CProgress,
  CSpinner,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableRow,
} from '@coreui/react'

import CIcon from '@coreui/icons-react'
import {
  cibFacebook,
  cibLinkedin,
  cibInstagram,
  cibGmail,
  cifId,
  cibGithub,
  cibGitlab,
  cibLaravel,
  cibReact,
  cibJquery,
  cibGoogleCloud,
  cibYoutube,
} from '@coreui/icons'

import mypicture from 'src/assets/images/me.jpg'
import ibnumasud from 'src/assets/images/ibnumasud.png'
import pragma from 'src/assets/images/pragma.png'
import rakitek from 'src/assets/images/rakitek.jpg'

const Dashboard = () => {
  const profile = [
    {
      name: 'Date Of Birth',
      user: {
        data: 'September 22th, 1998',
      },
    },
    {
      name: 'Nationality',
      user: {
        data: 'Indonesia',
      },
    },
    {
      name: 'Marital Status',
      user: {
        data: 'Single',
      },
    },
    {
      name: 'Hobby',
      user: {
        data: 'Football & Coding',
      },
    },
    {
      name: 'Address',
      user: {
        data: 'Jl Gulama, Tangkerang Barat, Pekanbaru'
      }
    }
  ]
  const edu = [
    {
      title: 'Kindergarden',
      name: 'TK Aisiyyah Kp.Panjang',
      jurusan: '',
      ipk: '',
      year: '2004-2005'
    },
    {
      title: 'Elementary School',
      name: 'SDN 015 KOTO PERAMBAHAN',
      jurusan: '',
      ipk: '',
      year: '2005-2011'
    },
    {
      title: 'Junior High School',
      name: 'MTsN Kampar',
      jurusan: '',
      ipk: '',
      year: '2011-2014'
    },
    {
      title: 'Senior High School',
      name: 'SMAN 1 Kampar Timur',
      jurusan: '',
      ipk: '',
      year: '2014-2017'
    },
    {
      title: 'University',
      name: 'Sultan Syarif Kasim State Islamic University, Riau',
      jurusan: 'Informatics Engineering',
      ipk: '3.51',
      year: '2017-2021'
    },
  ]
  const medsos = [
    {
      name: 'E-Mail',
      icon: cibGmail,
      link: '',
      data: 'muhammadzakie22g@gmail.com'
    },
    {
      name: 'Linkedin',
      icon: cibLinkedin,
      link: 'https://www.linkedin.com/in/muhammadzakie220998/',
      data: 'Muhammad Zakie'
    },
    {
      name: 'Instagram',
      icon: cibInstagram,
      link: 'https://www.instagram.com/muhammadzakie22/',
      data: 'muhammadzakie22'
    },
    {
      name: 'Facebook',
      icon: cibFacebook,
      link: '',
      data: 'Muhammad Zakie'
    },
  ]
  const experience = [
    {
      logo: ibnumasud,
      name: "Pondok Pesantren Ibnu Mas'ud",
      lokasi: "Kampar, Riau, Indonesia",
      type: "Magang",
      posisi: "Web Application Developer",
      masa: "January 1st, 2020 - March 1st, 2020",
      stack: "Codeigniter, Mysql"
    },
    {
      logo: pragma,
      name: "PT. Pragma Informatika",
      lokasi: "Bandung, West Java, Indonesia",
      type: "Fulltime WFO",
      posisi: "Web Developer",
      masa: "October 15th, 2021 - March 31th, 2021",
      stack: "Laravel, Mysql"
    },
    {
      logo: rakitek,
      name: "PT. Ruang Kreasi Inovasi Teknologi",
      lokasi: "South Jakarta, Jakarta, Indonesia",
      type: "Fulltime WFH",
      posisi: "Web Programmer",
      masa: "April 1st, 2022 - March 1st 2023",
      stack: "Laravel, Mysql, JQuery, Ajax, Vue, Flutter"
    },
  ]
  const skills = [
    {
      name: "Laravel",
      logo: cibLaravel,
      value: 75,
      color: "danger"
    },
    {
      name: "React JS",
      logo: cibReact,
      value: 40,
      color: "primary"
    },
    {
      name: "Vue JS",
      logo: '',
      value: 45,
      color: "primary"
    },
    {
      name: "JQuery",
      logo: cibJquery,
      value: 70,
      color: "secondary"
    },
    {
      name: "Ajax",
      logo: '',
      value: 65,
      color: "info"
    },
  ]
  return (
    <>
      <CCard className="mb-4">
        <CCardHeader></CCardHeader>
        <CCardBody>
          <CTable align="middle" className="mb-0 border" hover responsive>
            <CTableBody>
              <CTableRow>
                <CTableDataCell colSpan={2}>
                  <h1 className="mt-2 text-center">
                    <CSpinner color="danger" variant="grow"/>
                    <CSpinner color="warning" variant="grow"/>
                    <CSpinner color="success" variant="grow"/>
                      MUHAMMAD ZAKIE
                    <CSpinner color="info" variant="grow"/>
                    <CSpinner color="light" variant="grow"/>
                    <CSpinner color="dark" variant="grow"/>
                  </h1>
                </CTableDataCell>
                <CTableDataCell rowSpan={8}>
                  <CImage height={450} src={mypicture}></CImage>
                </CTableDataCell>
              </CTableRow>
              <CTableRow>
                <CTableDataCell colSpan={2}>
                  <h6 className="mt-2 text-center">
                    <CSpinner color="danger" size="sm" variant="grow"/>
                    <CSpinner color="warning" size="sm" variant="grow"/>
                    <CSpinner color="success" size="sm" variant="grow"/>
                      {' '}Web Developer{' '}
                    <CSpinner color="info" size="sm" variant="grow"/>
                    <CSpinner color="light" size="sm" variant="grow"/>
                    <CSpinner color="dark" size="sm" variant="grow"/>
                  </h6>
                </CTableDataCell>
              </CTableRow>
              <CTableRow>
                <CTableDataCell colSpan={2}>
                  <div className="text-justify">
                    <CSpinner color="info" size="sm"/>
                      {' '}I am Zakie, a bachelor of informatics engineering at the Sultan Syarif Kasim State Islamic University, Riau, Indonesia. my thesis is about implementing blockchain technology using the solidity programming language and the react js javascript library.</div>
                </CTableDataCell>
              </CTableRow>
              {profile.map((item, index) => (
                <CTableRow key={index}>
                  <CTableDataCell width={200}>
                    <div>{item.name}</div>
                  </CTableDataCell>
                  <CTableDataCell>
                    <div>:{' '}
                      {item.user.data}
                      {item.user.data==="Indonesia"?<CIcon size="xl" icon={cifId} title="Indonesia" />:''}
                    </div>
                  </CTableDataCell>
                </CTableRow>
              ))}
            </CTableBody>
          </CTable>
          <CTable align="middle" className="mb-0 border" hover responsive>
            <CTableBody>
              <CTableRow className="text-center">
                <CTableDataCell colSpan={3} style={{backgroundColor:'#ebedef'}}>
                  <h6>Social Media</h6>
                </CTableDataCell>
              </CTableRow>
              {medsos.map((item, index) => (
                <CTableRow key={index}>
                  <CTableDataCell width={100} className="text-center">
                    <CNavLink href={item.link} target="_blank">
                    <CIcon className="me-2 text-medium-emphasis" icon={item.icon} size="lg" />
                    </CNavLink>
                    <span>{item.name}</span>
                  </CTableDataCell>
                  <CTableDataCell>:</CTableDataCell>
                  <CTableDataCell>
                    {item.data}
                    {item.link != ''?
                    <div className='small text-medium-emphasis'>
                      <span>{item.link}</span>
                    </div>
                    :''}
                  </CTableDataCell>
                </CTableRow>
              ))}
            </CTableBody>
          </CTable>
          <CTable align="middle" className="mb-0 border" hover responsive>
            <CTableBody>
              <CTableRow className="text-center">
                <CTableDataCell width={300}>
                  <div>
                    <CNavLink href="https://gitlab.com/zakiepragma" target="_blank">
                      <CIcon className="me-2 text-medium-emphasis" icon={cibGitlab} size="xl" />
                    </CNavLink>
                    zakiepragma
                    <div className='small text-medium-emphasis'>
                      <span target="_blank">https://gitlab.com/zakiepragma</span>
                    </div>
                  </div>
                </CTableDataCell>
                <CTableDataCell width={300}>
                  <div>
                    <CNavLink href="https://github.com/programmercintasunnah" target="_blank">
                      <CIcon className="me-2 text-medium-emphasis" icon={cibGithub} size="xl" />
                    </CNavLink>
                    programmercintasunnah
                    <div className='small text-medium-emphasis'>
                      <span>https://github.com/programmercintasunnah</span>
                    </div>
                  </div>
                </CTableDataCell>
              </CTableRow>
              <CTableRow className="text-center">
                <CTableDataCell width={300}>
                  <div>
                    Click to View CV (Curriculum Vitae)
                    <CNavLink href="https://drive.google.com/drive/folders/15O8n-smVbigJYJV2BbfdRZ2feBKaprLc?usp=sharing" target="_blank">
                    <CIcon className="me-2 text-medium-emphasis" icon={cibGoogleCloud} size="xl" />
                    </CNavLink>
                  </div>
                </CTableDataCell>
                <CTableDataCell width={300}>
                  <div>
                    Click to View My Project
                    <CNavLink href="https://bit.ly/3LtDCud" target="_blank">
                    <CIcon className="me-2 text-medium-emphasis" icon={cibYoutube} size="xl" />
                    </CNavLink>
                  </div>
                </CTableDataCell>
                </CTableRow>
            </CTableBody>
          </CTable>
          <CTable align="middle" className="mb-0 border" hover responsive>
            <CTableBody>
              <CTableRow>
                <CTableDataCell colSpan={3} style={{backgroundColor:'#ebedef'}} className="text-center">
                  <h6>Education</h6>
                </CTableDataCell>
              </CTableRow>
              {edu.map((item, index) => (
                <CTableRow key={index}>
                  <CTableDataCell width={200}>
                    <div>{item.title}</div>
                  </CTableDataCell>
                  <CTableDataCell>:</CTableDataCell>
                  <CTableDataCell>
                    <div>{item.name}</div>
                    <div className="small text-medium-emphasis">
                      <span>
                        Year : {item.year}
                        {item.jurusan != "" ? " | Bachelor Degree "+item.jurusan+" | ": ''}
                        {item.ipk != "" ? "GPA : "+item.ipk : ''}
                      </span>
                    </div>
                  </CTableDataCell>
                </CTableRow>
              ))}
            </CTableBody>
          </CTable>
          <CTable align="middle" className="mb-0 border" hover responsive>
            <CTableBody>
              <CTableRow>
                <CTableDataCell colSpan={3} style={{backgroundColor:'#ebedef'}} className="text-center">
                  <h6>Experience</h6>
                </CTableDataCell>
              </CTableRow>
              <CTableRow>
              {experience.map((item, index) => (
                <CTableDataCell key={index}>
                  <CCard style={{ width: '18rem' }}>
                    <CCardImage orientation="top" src={item.logo} />
                    <CCardBody>
                      <CCardText>
                        <h5>{item.name}</h5>
                        <div>{item.lokasi}</div>
                        <div>{item.posisi}</div>
                        <div>{item.type}</div>
                        <div>{item.stack}</div>
                      </CCardText>
                    </CCardBody>
                    <CCardFooter>
                      <small className="text-medium-emphasis">{item.masa}</small>
                    </CCardFooter>
                  </CCard>
                </CTableDataCell>
              ))}
              </CTableRow>
            </CTableBody>
          </CTable>
          <CTable align="middle" className="mb-0 border" hover responsive>
            <CTableBody>
              <CTableRow>
                <CTableDataCell colSpan={3} style={{backgroundColor:'#ebedef'}} className="text-center">
                  <h6>Skills</h6>
                </CTableDataCell>
              </CTableRow>
              {skills.map((item, index) => (
                <CTableRow v-for="item in tableItems" key={index}>
                  <CTableDataCell className="text-center" width={10}>
                    <CIcon icon={item.logo} size="lg" />
                  </CTableDataCell>
                  <CTableDataCell width={100}>
                    <div>{item.name}</div>
                  </CTableDataCell>
                  <CTableDataCell>
                    <div className="clearfix">
                      <div className="float-start">
                        <strong>{item.value}%</strong>
                      </div>
                    </div>
                    <CProgress thin color={item.color} value={item.value} />
                  </CTableDataCell>
                </CTableRow>
              ))}
            </CTableBody>
          </CTable>
        </CCardBody>
      </CCard>
    </>
  )
}

export default Dashboard
