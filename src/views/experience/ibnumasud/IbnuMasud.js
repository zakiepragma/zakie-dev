import React from 'react'

import {
  CAccordion,
  CAccordionBody,
  CAccordionHeader,
  CAccordionItem,
  CCard,
  CCardBody,
  CCardHeader,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableRow,
  CImage,
  CNavLink,
} from '@coreui/react'
import ibnumasud from 'src/assets/images/ibnumasud.png'
import project from 'src/assets/images/login.png'

import CIcon from '@coreui/icons-react'
import {
  cibGithub,
  cibGoogleCloud,
  cibYoutube,
} from '@coreui/icons'

const IbnuMasud = () => {
  return (
    <>
      <CCard className="mb-4">
        <CCardHeader></CCardHeader>
        <CCardBody>
        <CAccordion flush>
          <CAccordionItem itemKey={1}>
            <CAccordionHeader>
              Profile
            </CAccordionHeader>
            <CAccordionBody>
            <CTable align="middle text-center" className="mb-0 border" hover responsive>
              <CTableBody>
                <CTableRow>
                  <CTableDataCell>
                    <CImage height={300} src={ibnumasud}></CImage>
                  </CTableDataCell>
                </CTableRow>
                <CTableRow>
                  <CTableDataCell>
                  <div className='text-justify'>
                  Pondok Pesantren Tahfidz Al-Qur’an Ibnu Mas’ud Kampar atau lebih dikenal
                  dengan sebutan PPS Ibnu Mas’ud merupakan salah satu pondok pesantren tahfidz
                  al-qur’an yang bermanhaj salafus-sholeh di kabupaten kampar dibawah yayasan
                  nida’ as-sunnah. Sekolah ini berdiri tahun 2011 M/1432 H, jadi sekolah ini masih
                  tergolong baru dan membutuhkan penerapan, salah satunya adalah untuk absensi
                  kehadiran pegawai dan kegiatan keluar pegawai yang mana di situ dikenal dengan
                  sebutan jurnal. Pada umumnya dalam absensi kehadiran dan isi jurnal masih
                  menggunakan kertas. Pada sekolah ini jumlah tidak hadir dan jumlah menit keluar
                  dari pekarangan sekolah akan ada pemotongan gaji pegawai dan akan ada rencana
                  jumlah menit terlambat juga akan ada pemotongan gaji. Untuk mengatasi masalah
                  tersebut maka dibangunlah Sistem Informasi Kegiatan Keluar Pegawai dan Absensi
                  Kehadiran Pegawai dengan QR-Code yang akan menggunakan metode waterfall
                  dan Bahasa pemrograman PHP dengan DBMS MySQL.
                  </div>
                  </CTableDataCell>
                </CTableRow>
              </CTableBody>
            </CTable>
            </CAccordionBody>
          </CAccordionItem>
          <CAccordionItem itemKey={2}>
            <CAccordionHeader>
              Application
            </CAccordionHeader>
            <CAccordionBody>
              <CTable align="middle text-center" className="mb-0 border" hover responsive>
              <CTableBody>
                <CTableRow>
                  <CTableDataCell>
                    <CImage height={500} src={project}></CImage>
                    <h4 className='mt-5 mb-5'>
                    RANCANG BANGUN SISTEM INFORMASI KEGIATAN
                    KELUAR PEGAWAI DAN ABSENSI KEHADIRAN PEGAWAI
                    DENGAN QR-CODE DENGAN CODEIGNITER
                    (STUDI KASUS: PPS IBNU MAS’UD KAMPAR)
                    </h4>
                  </CTableDataCell>
                </CTableRow>
                <CTableRow>
                  <CTableDataCell>
                  <div>
                    <CNavLink href="https://github.com/programmercintasunnah/kp-ibnu_masud_kampar-ci" target="_blank">
                      <CIcon className="me-2 text-medium-emphasis" icon={cibGithub} size="xl" />
                    </CNavLink>
                    Source Code
                    <div className='small text-medium-emphasis'>
                      <span target="_blank">https://github.com/programmercintasunnah/kp-ibnu_masud_kampar-ci</span>
                    </div>
                  </div>
                  </CTableDataCell>
                </CTableRow>
                <CTableRow>
                  <CTableDataCell>
                  <div>
                    <CNavLink href="https://drive.google.com/file/d/1qmRCHve8qW3IZ11KZreBczAQFuaUd55z/view?usp=sharing" target="_blank">
                      <CIcon className="me-2 text-medium-emphasis" icon={cibGoogleCloud} size="xl" />
                    </CNavLink>
                    Laporan Kerja Praktek (Magang)
                    <div className='small text-medium-emphasis'>
                      <span target="_blank">https://drive.google.com/file/d/1qmRCHve8qW3IZ11KZreBczAQFuaUd55z/view?usp=sharing</span>
                    </div>
                  </div>
                  </CTableDataCell>
                </CTableRow>
                <CTableRow>
                  <CTableDataCell>
                  <div>
                    <CNavLink href="https://www.youtube.com/watch?v=OCB_eXbcEJc&list=PLGwA21JLpwoMtXpnQHnhv0kasbL2tXWlt&index=12" target="_blank">
                      <CIcon className="me-2 text-medium-emphasis" icon={cibYoutube} size="xl" />
                    </CNavLink>
                    Demo (YouTube)
                    <div className='small text-medium-emphasis'>
                      <span target="_blank">https://www.youtube.com/watch?v=OCB_eXbcEJc&list=PLGwA21JLpwoMtXpnQHnhv0kasbL2tXWlt&index=12</span>
                    </div>
                  </div>
                  </CTableDataCell>
                </CTableRow>
              </CTableBody>
            </CTable>
            </CAccordionBody>
          </CAccordionItem>
          <CAccordionItem itemKey={3}>
            <CAccordionHeader>
              Documentation
            </CAccordionHeader>
            <CAccordionBody className='text-center'>
              <CNavLink href="https://drive.google.com/drive/folders/1i2_93PzA_nvZKagSAYdxwfaMu4s5L6ql?usp=sharing" target='_blank'>
                <CIcon className="me-2 text-medium-emphasis" icon={cibGoogleCloud} size="xl" />
              </CNavLink>
              <span className='small text-medium-emphasis'>Klik Icon untuk ke dokumentasi</span>
              <div className='small text-medium-emphasis'>
                <span target="_blank">https://drive.google.com/drive/folders/1i2_93PzA_nvZKagSAYdxwfaMu4s5L6ql?usp=sharing</span>
              </div>
            </CAccordionBody>
          </CAccordionItem>
        </CAccordion>
        <span className='small text-medium-emphasis'>magang dari 1 januari sampai 1 februari 2020</span>
        </CCardBody>
      </CCard>
    </>
  )
}

export default IbnuMasud
