import React from 'react'

import {
  CCard,
  CCardBody,
  CCardHeader,
  CAccordion,
  CAccordionBody,
  CAccordionHeader,
  CAccordionItem,
  CCarousel,
  CCarouselItem,
  CCarouselCaption,
  CImage,
  CTableBody,
  CTable,
  CTableDataCell,
  CTableRow,
  CNavLink,
} from '@coreui/react'

import w1 from 'src/assets/images/w1.png'
import w2 from 'src/assets/images/w2.png'
import w3 from 'src/assets/images/w3.png'
import w4 from 'src/assets/images/w4.png'

import pragma from 'src/assets/images/pragma.png'
import hsse from 'src/assets/images/hsse-kal.png'
import CIcon from '@coreui/icons-react'
import { cilPaperclip } from '@coreui/icons'

const waskita = [
  {
    img: w1,
  },
  {
    img: w2,
  },
  {
    img: w3,
  },
  {
    img: w4,
  },
]

const Pragma = () => {
  return (
    <>
      <CCard className="mb-4">
        <CCardHeader></CCardHeader>
        <CCardBody>
          <CAccordion flush>
            <CAccordionItem itemKey={1}>
              <CAccordionHeader>
                Profile
              </CAccordionHeader>
              <CAccordionBody>
              <CTable align="middle text-center" className="mb-0 border" hover responsive>
                <CTableBody>
                  <CTableRow>
                    <CTableDataCell>
                      <CImage height={300} src={pragma}></CImage>
                    </CTableDataCell>
                  </CTableRow>
                  <CTableRow>
                    <CTableDataCell>
                    <div className='text-justify'>
                      PT. Pragma Informatika adalah perusahaan yang bergerak di bidang IT yang berlokasi di Kabupaten Bandung, Provinsi Jawa Barat, Indonesia.
                      <CNavLink target='_blank' href='https://www.pragmainf.co.id/'>https://www.pragmainf.co.id/</CNavLink>
                    </div>
                    </CTableDataCell>
                  </CTableRow>
                </CTableBody>
              </CTable>
              </CAccordionBody>
            </CAccordionItem>
          </CAccordion>
          <CAccordion flush>
            <CAccordionItem itemKey={2}>
              <CAccordionHeader>
                Project HSSE KALIMANTAN BARAT
              </CAccordionHeader>
              <CAccordionBody>
              <CTable align="middle text-center" className="mb-0 border" hover responsive>
                <CTableBody>
                  <CTableRow>
                    <CTableDataCell>
                      <CImage height={500} src={hsse}></CImage>
                    </CTableDataCell>
                  </CTableRow>
                  <CTableRow>
                    <CTableDataCell>
                    <h4>
                      HSSE UP2B KALBAR V.2
                    </h4>
                    </CTableDataCell>
                    </CTableRow>
                      <CTableRow>
                      <CTableDataCell>
                      <div>
                        <CNavLink href="https://drive.google.com/file/d/1pF32Rd0_fX_Q19uol-4lpMNTb3OVH7vH/view?usp=sharing" target="_blank">
                          <CIcon className="me-2 text-medium-emphasis" icon={cilPaperclip} size="xl" />
                        </CNavLink>
                        Documentation PDF
                        <div className='small text-medium-emphasis'>
                          <span target="_blank">https://drive.google.com/file/d/1pF32Rd0_fX_Q19uol-4lpMNTb3OVH7vH/view?usp=sharing</span>
                        </div>
                      </div>
                      </CTableDataCell>
                    </CTableRow>
                  </CTableBody>
                </CTable>
              </CAccordionBody>
            </CAccordionItem>
          </CAccordion>
          <CAccordion flush>
            <CAccordionItem itemKey={3}>
              <CAccordionHeader>
                Project HSSE KALIMANTAN
              </CAccordionHeader>
              <CAccordionBody>
                <CTable align="middle text-center" className="mb-0 border" hover responsive>
                  <CTableBody>
                    <CTableRow>
                      <CTableDataCell>
                        <div>
                        <CNavLink href="https://drive.google.com/file/d/1pF32Rd0_fX_Q19uol-4lpMNTb3OVH7vH/view?usp=sharing" target="_blank">
                          <CIcon className="me-2 text-medium-emphasis" icon={cilPaperclip} size="xl" />
                        </CNavLink>
                        Documentation PDF
                        <div className='small text-medium-emphasis'>
                          <span target="_blank">https://drive.google.com/file/d/1pF32Rd0_fX_Q19uol-4lpMNTb3OVH7vH/view?usp=sharing</span>
                        </div>
                        </div>
                      </CTableDataCell>
                    </CTableRow>
                  </CTableBody>
                </CTable>
              </CAccordionBody>
            </CAccordionItem>
          </CAccordion>
          <CAccordion flush>
            <CAccordionItem itemKey={4}>
              <CAccordionHeader>
                Project WELCOME WASKITA
              </CAccordionHeader>
              <CAccordionBody>
              <CCarousel controls indicators dark>
              {waskita.map((item, index) => (
                <CCarouselItem>
                  <img className="d-block w-100" src={item.img} alt="slide 1" />
                  <CCarouselCaption className="d-none d-md-block">
                    <h5>Welcome Waskita</h5>
                    <p>Menu Laporan Peninjauan Lapangan</p>
                  </CCarouselCaption>
                </CCarouselItem>
              ))}
              </CCarousel>
              </CAccordionBody>
            </CAccordionItem>
          </CAccordion>
        <span className='small text-medium-emphasis'>kerja dari 15 oktober 2021 sampai 31 maret 2022</span>
        </CCardBody>
      </CCard>
    </>
  )
}

export default Pragma
