import React from 'react'

import {
  CAccordion,
  CAccordionBody,
  CAccordionHeader,
  CAccordionItem,
  CCard,
  CCardBody,
  CCardHeader,
  CImage,
  CNavLink,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableRow,
} from '@coreui/react'
import rakitek2 from 'src/assets/images/rakitek.png'
import rakitek from 'src/assets/images/rakitek.jpg'

import rakomsis15 from 'src/assets/images/rakomsis1-5.png'
import rakomsis30 from 'src/assets/images/rakomsis3-0.png'

import portal_rakomsis1 from 'src/assets/images/portal_rakomsis1.png'
import portal_rakomsis2 from 'src/assets/images/portal_rakomsis2.png'
import portal_rakomsis3 from 'src/assets/images/portal_rakomsis3.png'
import portal_rakomsis4 from 'src/assets/images/portal_rakomsis4.png'
import portal_rakomsis5 from 'src/assets/images/portal_rakomsis5.png'
import portal_rakomsis6 from 'src/assets/images/portal_rakomsis6.png'

import { cilBrowser } from '@coreui/icons'
import CIcon from '@coreui/icons-react'

const Rakitek = () => {
  return (
    <>
      <CCard className="mb-4">
        <CCardHeader></CCardHeader>
        <CCardBody>
        <CAccordion flush>
            <CAccordionItem itemKey={1}>
              <CAccordionHeader>
                Profile
              </CAccordionHeader>
              <CAccordionBody>
                <CTable align="middle text-center" className="mb-0 border" hover responsive>
                  <CTableBody>
                    <CTableRow>
                      <CTableDataCell>
                        <CImage height={300} src={rakitek}></CImage>
                      </CTableDataCell>
                    </CTableRow>
                    <CTableRow>
                      <CTableDataCell>
                      <div className='text-justify'>
                        PT. Ruang Kreasi Inovasi Teknologi adalah perusahaan yang bergerak di bidang IT yang berlokasi di Jakarta Selatan, Jakarta, Indonesia.
                        <CNavLink target='_blank' href='https://rakitek.com/'>https://rakitek.com/</CNavLink>
                      </div>
                      </CTableDataCell>
                    </CTableRow>
                  </CTableBody>
                </CTable>
              </CAccordionBody>
            </CAccordionItem>
          </CAccordion>
          <CAccordion flush>
            <CAccordionItem itemKey={2}>
              <CAccordionHeader>
                Project RAKOMSIS V.3.0
              </CAccordionHeader>
              <CAccordionBody>
                <CTable align="middle text-center" className="mb-0 border" hover responsive>
                  <CTableBody>
                    <CTableRow>
                      <CTableDataCell>
                        <CImage height={250} src={rakomsis15}></CImage>
                        <h6 className='mt-3'>RAKOMSIS V.1.5</h6>
                      </CTableDataCell>
                      <CTableDataCell>
                        <CImage height={250} src={rakomsis30}></CImage>
                        <h6 className='mt-3'>RAKOMSIS V.3.0</h6>
                      </CTableDataCell>
                    </CTableRow>
                    <CTableRow>
                    <CTableDataCell colSpan={2}>
                    <div>
                      <CNavLink href="https://rakomsis.com/" target="_blank">
                        <CIcon className="me-2 text-medium-emphasis" icon={cilBrowser} size="xl" />
                      </CNavLink>
                      Documentation
                      <div className='small text-medium-emphasis'>
                        <span>https://rakomsis.com/</span>
                      </div>
                    </div>
                    </CTableDataCell>
                  </CTableRow>
                  </CTableBody>
                </CTable>
              </CAccordionBody>
            </CAccordionItem>
          </CAccordion>
          <CAccordion flush>
            <CAccordionItem itemKey={2}>
              <CAccordionHeader>
                Project Portal RAKOMSIS
              </CAccordionHeader>
              <CAccordionBody>
                <CTable align="middle text-center" className="mb-0 border" hover responsive>
                  <CTableBody>
                    <CTableRow>
                      <CTableDataCell>
                        <CImage height={250} src={portal_rakomsis1}></CImage>
                      </CTableDataCell>
                      <CTableDataCell>
                        <CImage height={250} src={portal_rakomsis2}></CImage>
                      </CTableDataCell>
                    </CTableRow>
                    <CTableRow>
                      <CTableDataCell>
                        <CImage height={250} src={portal_rakomsis3}></CImage>
                      </CTableDataCell>
                      <CTableDataCell>
                        <CImage height={250} src={portal_rakomsis4}></CImage>
                      </CTableDataCell>
                    </CTableRow>
                    <CTableRow>
                      <CTableDataCell>
                        <CImage height={250} src={portal_rakomsis5}></CImage>
                      </CTableDataCell>
                      <CTableDataCell>
                        <CImage height={250} src={portal_rakomsis6}></CImage>
                      </CTableDataCell>
                    </CTableRow>
                    <CTableRow>
                    <CTableDataCell colSpan={2}>
                    {/* <div>
                      <CNavLink href="https://rakomsis.com/" target="_blank">
                        <CIcon className="me-2 text-medium-emphasis" icon={cilBrowser} size="xl" />
                      </CNavLink>
                      Documentation
                      <div className='small text-medium-emphasis'>
                        <span>https://rakomsis.com/</span>
                      </div>
                    </div> */}
                    </CTableDataCell>
                  </CTableRow>
                  </CTableBody>
                </CTable>
              </CAccordionBody>
            </CAccordionItem>
          </CAccordion>
        <span className='small text-medium-emphasis'>kerja dari 1 april 2022 sampai sekarang</span>
        </CCardBody>
      </CCard>
    </>
  )
}

export default Rakitek
