import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCarousel,
  CCarouselItem,
  CCol,
  CRow,
} from '@coreui/react'

import c0 from 'src/assets/images/certificate1.png'
import c1 from 'src/assets/images/certificate2.png'
import c2 from 'src/assets/images/certificate3.png'
import c3 from 'src/assets/images/certificate4.png'
import c4 from 'src/assets/images/certificate5.png'
import c5 from 'src/assets/images/certificate6.png'
import c6 from 'src/assets/images/certificate7.png'

const slidesLight = [
  c0,c1,c2,c3,c4,c5,c6
]

const Certificate = () => {
  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Certificate</strong>
          </CCardHeader>
          <CCardBody>
            <CCarousel controls indicators dark>
            {slidesLight.map((item, index) => (
              <CCarouselItem>
                <img className="d-block w-100" src={item} alt="slide 1" />
              </CCarouselItem>
            ))}
            </CCarousel>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Certificate