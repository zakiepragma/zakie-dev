import React from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCarousel,
  CCarouselCaption,
  CCarouselItem,
  CCol,
  CForm,
  CFormCheck,
  CFormInput,
  CFormSelect,
  CFormTextarea,
  CRow,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from '@coreui/react'
import { DocsCallout, DocsExample } from 'src/components'

import c0 from 'src/assets/images/carousel4.jpg'
import c1 from 'src/assets/images/carousel1.jpg'
import c2 from 'src/assets/images/carousel2.JPG'
import c3 from 'src/assets/images/carousel3.png'
import c4 from 'src/assets/images/carousel0.jpg'
import c5 from 'src/assets/images/carousel5.jpg'
import c6 from 'src/assets/images/carousel6.JPG'
import c7 from 'src/assets/images/carousel7.jpg'

const slidesLight = [
  c0,c1,c2,c3,c4,c5,c6,c7
]

const caro1 = [
  {
    img: c0,
    title: "Ngoding",
    caption: "Koto Perambahan, -",
  },
  {
    img: c1,
    title: "Payakumbuh",
    caption: "25 Desember 2020",
  },
  {
    img: c2,
    title: "Air Terjun Osang",
    caption: "Kampar, January 14th, 2020",
  },
  {
    img: c3,
    title: "UIN SUSKA RIAU",
    caption: "Pekanbaru, August 13th, 2017",
  },
  {
    img: c4,
    title: "Air Terjun Osang",
    caption: "Kampar, January 14th, 2020",
  },
  {
    img: c5,
    title: "Diatas Ketinggian",
    caption: "Koto Perambahan, -",
  },
  {
    img: c6,
    title: "Air Terjun Osang",
    caption: "Kampar, January 14th, 2020",
  },
  {
    img: c7,
    title: "Futsal TIF USR",
    caption: "Pekanbaru, November 3th, 2019",
  },
]

const family = [
  {
    name: "Father",
    kelahiran: "1971",
    agama: "Islam",
    pekerjaan: "Entrepreneur (Wiraswasta)",
    pendidikan: "High school level (SMA)",
    status: "Still Alive (Masih Hidup)",
  },
  {
    name: "Mother",
    kelahiran: "1974",
    agama: "Islam",
    pekerjaan: "Housewife (IRT)",
    pendidikan: "High school level (SMA)",
    status: "Still Alive (Masih Hidup)",
  },
  {
    name: "Me",
    kelahiran: "1998",
    agama: "Islam",
    pekerjaan: "Programmer",
    pendidikan: "Bachelor (S1)",
    status: "Still Alive (Masih Hidup)",
  },
  {
    name: "Brother 1",
    kelahiran: "2002",
    agama: "Islam",
    pekerjaan: "-",
    pendidikan: "High school level (SMA)",
    status: "Still Alive (Masih Hidup)",
  },
  {
    name: "Brother 2",
    kelahiran: "2008",
    agama: "Islam",
    pekerjaan: "-",
    pendidikan: "Junior school level (SMP)",
    status: "Still Alive (Masih Hidup)",
  },
  {
    name: "Sister 1",
    kelahiran: "2015",
    agama: "Islam",
    pekerjaan: "-",
    pendidikan: "Elementary school level (SD)",
    status: "Still Alive (Masih Hidup)",
  },
]

const Profile = () => {
  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Profile</strong>
          </CCardHeader>
          <CCardBody>
            <CCarousel controls indicators dark>
            {caro1.map((item, index) => (
              <CCarouselItem>
                <img className="d-block w-100" src={item.img} alt="slide 1" />
                <CCarouselCaption className="d-none d-md-block">
                  <h5>{item.title}</h5>
                  <p>{item.caption}.</p>
                </CCarouselCaption>
              </CCarouselItem>
            ))}
            </CCarousel>
            <CForm className="row g-3 mt-3">
              <CCol md={9}>
                <CFormInput disabled label="Full Name (Nama Lengkap)" value="Muhammad Zakie"/>
              </CCol>
              <CCol md={3}>
                <CFormInput disabled label="Nickname (Panggilan)" value="Zakie"/>
              </CCol>
              <CCol md={8}>
                <CFormInput disabled label="Place, Date of Birth (Tempat, Tanggal Lahir)" value="PD Merbau, September 22th, 1998"/>
              </CCol>
              <CCol md={4}>
                <CFormInput disabled label="ID Card (No. KTP)" value="1**************1"/>
              </CCol>
              <CCol md={6}>
                <CFormInput disabled label="Phone" value="+6282286250694"/>
              </CCol>
              <CCol md={6}>
                <CFormInput disabled label="E-Mail" value="muhammadzakie22g@gmail.com"/>
              </CCol>
              <CCol md={3}>
                <CFormSelect disabled label="Gender (Jenis Kelamin)">
                  <option selected>Male (Pria)</option>
                  <option>Female (Wanita)</option>
                </CFormSelect>
              </CCol>
              <CCol md={3}>
                <CFormInput disabled label="Religion (Agama)" value="Islam"/>
              </CCol>
              <CCol md={3}>
                <CFormInput disabled label="Manhaj" value="Salaf"/>
              </CCol>
              <CCol md={3}>
                <CFormInput disabled label="Ethnic Group (Suku)" value="Domo"/>
              </CCol>
              <CCol md={4}>
                <CFormInput disabled label="Blood Group (Golongan Darah)" value="A"/>
              </CCol>
              <CCol md={4}>
                <CFormInput disabled label="Sibling (Saudara)" value="1 of 4"/>
              </CCol>
              <CCol md={4}>
                <CFormInput disabled label="Marital Status (Status Pernikahan)" value="Single"/>
              </CCol>
              <CCol md={6}>
                <CFormInput disabled label="Education Degree (Gelar)" value="Bachelor of Engineering (Sarjana Teknik)"/>
              </CCol>
              <CCol md={6}>
                <CFormInput disabled label="GPA (IPK)" value="3.51 of 4.0"/>
              </CCol>
              <CCol md={6}>
                <CFormTextarea style={{ height: '100px' }} disabled label="Address according to ID Card (Alamat Sesuai KTP)" value="RT 01, RW 01, Dusun Perambahan, Desa Koto Perambahan, Kecamatan Kampa, Kabupaten Kampar, Provinsi Riau, Indonesia"/>
              </CCol>
              <CCol md={6}>
                <CFormTextarea style={{ height: '100px' }} disabled label="Current Residence Address (Tempat Tinggal Sekarang)" value="RT 01, RW 01, Dusun Perambahan, Desa Koto Perambahan, Kecamatan Kampa, Kabupaten Kampar, Provinsi Riau, Indonesia"/>
              </CCol>
              <CCol md={4}>
                <CFormInput disabled label="Physical Form (Bentuk Fisik)" value="Normal"/>
              </CCol>
              <CCol md={4}>
                <CFormInput disabled label="Skin Color (Warna Kulit)" value="Brown (Sawo Matang)"/>
              </CCol>
              <CCol md={4}>
                <CFormInput disabled label="Hair Color (Warna Rambut)" value="Black (Hitam)"/>
              </CCol>
              <CCol md={3}>
                <CFormInput disabled label="Hair Type (Tipe Rambut)" value="Straight (Lurus)"/>
              </CCol>
              <CCol md={3}>
                <CFormInput disabled label="Eye Color (Warna Mata)" value="Black (Hitam)"/>
              </CCol>
              <CCol md={3}>
                <CFormInput disabled label="Height (Tinggi)" value="170 Cm"/>
              </CCol>
              <CCol md={3}>
                <CFormInput disabled label="Weight (Berat)" value="55 Kg"/>
              </CCol>
              <CCol md={4}>
                <CFormInput disabled label="Favorite Sport (Olahraga Favorit)" value="Football, Badminton"/>
              </CCol>
              <CCol md={4}>
                <CFormInput disabled label="Disease history (Riwayat Penyakit)" value="There Isn't Any (Tidak Ada)"/>
              </CCol>
              <CCol md={4}>
                <CFormInput disabled label="Physical Disability (Cacat Fisik)" value="-"/>
              </CCol>
              <CCol md={6}>
                <CFormTextarea style={{ height: '70px' }} disabled label="Motto (Moto)" value="Be Grateful, Then Allah Will Add (Bersyukurlah, Maka Allah Tambah)"/>
              </CCol>
              <CCol md={6}>
                <CFormTextarea style={{ height: '70px' }} disabled label="Life Goal (Target Hidup)" value="Die to heaven without reckoning and without punishment (Mati masuk surga tanpa hisab dan tanpa adzab)"/>
              </CCol>
              <CCol md={6}>
                <CFormInput disabled label="Hobby" value="Football, Futsal, Badminton, Sport, Coding"/>
              </CCol>
              <CCol md={6}>
                <CFormInput disabled label="Free time activities (Kegiatan Waktu Luang)" value="Add knowledge or sports (Menambah ilmu atau olahraga)"/>
              </CCol>
              <CCol md={6}>
                <CFormTextarea style={{ height: '70px' }} disabled label="Likes (Hal disukai)" value="gentleness, honesty, friendliness (kelembutan, kejujuran, keramahan)"/>
              </CCol>
              <CCol md={6}>
                <CFormTextarea style={{ height: '70px' }} disabled label="Positive Traits (Sifat Positif)" value="trustworthy, honest, trustworthy (dipercaya, jujur, amanah)"/>
              </CCol>
              <CCol md={3}>
                <CFormInput disabled label="Negative Traits (Sifat Negatif)" value="Overthinking"/>
              </CCol>
              <CCol md={9}>
                <CFormInput disabled label="Smoking / Drinking Alcohol / Drinking Khamr (Merokok / Minum Alkohol / Minum Khamr)" value="No (Tidak)"/>
              </CCol>
              <h5>Family (Keluarga)</h5>
              <CTable align="middle text-center" className="mb-0 border" hover responsive>
              <CTableHead color="light">
                <CTableRow>
                  <CTableHeaderCell scope="col">#</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Name</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Birth</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Profession</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Education</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Status</CTableHeaderCell>
                </CTableRow>
              </CTableHead>
                <CTableBody>
                  {family.map((item, index) => (
                    <CTableRow v-for="item in tableItems" key={index}>
                       <CTableDataCell className="text-center" width={10}>
                        <div>{index+1}</div>
                      </CTableDataCell>
                      <CTableDataCell className="text-center" width={10}>
                        <div>{item.name}</div>
                      </CTableDataCell>
                      <CTableDataCell width={10}>
                        <div>{item.kelahiran}</div>
                      </CTableDataCell>
                      <CTableDataCell width={100}>
                        <div>{item.pekerjaan}</div>
                      </CTableDataCell>
                      <CTableDataCell width={120}>
                        <div>{item.pendidikan}</div>
                      </CTableDataCell>
                      <CTableDataCell width={100}>
                        <div>{item.status}</div>
                      </CTableDataCell>
                    </CTableRow>
                  ))}
                </CTableBody>
              </CTable>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Profile
