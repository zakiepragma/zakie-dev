import React from 'react'

import {
  CCard,
  CCardBody,
  CCardHeader,
  CCarousel,
  CCarouselCaption,
  CCarouselItem,
  CNavLink,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableRow,
} from '@coreui/react'

import dars0 from 'src/assets/images/dars0.png'
import dars00 from 'src/assets/images/dars00.png'
import dars1 from 'src/assets/images/dars1.png'
import dars2 from 'src/assets/images/dars2.png'
import dars3 from 'src/assets/images/dars3.png'
import dars4 from 'src/assets/images/dars4.png'
import dars5 from 'src/assets/images/dars5.png'
import dars6 from 'src/assets/images/dars6.png'
import dars7 from 'src/assets/images/dars7.png'
import dars8 from 'src/assets/images/dars8.png'
import dars9 from 'src/assets/images/dars9.png'
import dars10 from 'src/assets/images/dars10.png'
import dars11 from 'src/assets/images/dars11.png'
import CIcon from '@coreui/icons-react'
import { cibYoutube, cilBrowser } from '@coreui/icons'

const darsalafy = [
  {
    img: dars0,
    title: "HOME PAGE",
  },
  {
    img: dars00,
    title: "HOME PAGE",
  },
  {
    img: dars1,
    title: "LOGIN PAGE",
  },
  {
    img: dars2,
    title: "REGISTER PAGE",
  },
  {
    img: dars3,
    title: "DASHBOARD ADMIN",
  },
  {
    img: dars4,
    title: "PERSYARATAN ADMIN PAGE",
  },
  {
    img: dars5,
    title: "PENDAFTARAN ADMIN PAGE",
  },
  {
    img: dars6,
    title: "USER SANTRI PAGE",
  },
  {
    img: dars7,
    title: "USER TU PAGE",
  },
  {
    img: dars8,
    title: "PELANGGARAN ADMIN PAGE",
  },
  {
    img: dars9,
    title: "PENDAFTARAN TU PAGE",
  },
  {
    img: dars10,
    title: "DASHBOARD SANTRI PAGE",
  },
  {
    img: dars11,
    title: "VERIFICATION EMAIL SANTRI PAGE",
  },
]
const Darsalafiyah = () => {
  return (
    <>
      <CCard className="mb-4">
        <CCardHeader></CCardHeader>
        <CCardBody>
          <CCarousel controls indicators dark>
            {darsalafy.map((item, index) => (
              <CCarouselItem>
                <img className="d-block w-100" src={item.img} alt="slide 1" />
                <CCarouselCaption className="d-none d-md-block">
                  <h5>{item.title}</h5>
                </CCarouselCaption>
              </CCarouselItem>
            ))}
          </CCarousel>
          <CTable align="middle text-center" className="mb-0 border" hover responsive>
              <CTableBody>
                <CTableRow>
                  <CTableDataCell colSpan={3}>
                  <div>
                    <CNavLink href="http://sis.darsalafiyah.com/" target="_blank">
                      <CIcon className="me-2 text-medium-emphasis" icon={cilBrowser} size="xl" />
                    </CNavLink>
                    Link Application
                    <div className='small text-medium-emphasis'>
                      <span>http://sis.darsalafiyah.com/</span>
                    </div>
                  </div>
                  </CTableDataCell>
                </CTableRow>
                <CTableRow>
                  <CTableDataCell>
                  <div>
                    <CNavLink href="https://www.youtube.com/watch?v=6Mz97_9o9Ww&list=PLGwA21JLpwoMtXpnQHnhv0kasbL2tXWlt&index=26" target="_blank">
                      <CIcon className="me-2 text-medium-emphasis" icon={cibYoutube} size="xl" />
                    </CNavLink>
                    Demo 1 (YouTube)
                    <div className='small text-medium-emphasis'>
                      <span>klik icon youtube</span>
                    </div>
                  </div>
                  </CTableDataCell>
                  <CTableDataCell>
                  <div>
                    <CNavLink href="https://www.youtube.com/watch?v=Y0H0mE7x_TQ&list=PLGwA21JLpwoMtXpnQHnhv0kasbL2tXWlt&index=20" target="_blank">
                      <CIcon className="me-2 text-medium-emphasis" icon={cibYoutube} size="xl" />
                    </CNavLink>
                    Demo 2 (YouTube)
                    <div className='small text-medium-emphasis'>
                      <span>klik icon youtube</span>
                    </div>
                  </div>
                  </CTableDataCell>
                  <CTableDataCell>
                  <div>
                    <CNavLink href="-" target="_blank">
                      <CIcon className="me-2 text-medium-emphasis" icon={cibYoutube} size="xl" />
                    </CNavLink>
                    Demo 3 (YouTube)
                    <div className='small text-medium-emphasis'>
                      <span>-</span>
                    </div>
                  </div>
                  </CTableDataCell>
                </CTableRow>
              </CTableBody>
            </CTable>
        </CCardBody>
      </CCard>
    </>
  )
}

export default Darsalafiyah
