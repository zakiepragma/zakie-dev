import React from 'react'

import {
  CCard,
  CCardBody,
  CCardHeader,
  CImage,
  CNavLink,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLink } from '@coreui/icons'

import ta1 from 'src/assets/images/ta1.png'
import ta2 from 'src/assets/images/ta2.JPG'
import ta3 from 'src/assets/images/ta3.png'

const Ta = () => {
  return (
    <>
      <CCard className="mb-4">
        <CCardHeader></CCardHeader>
        <CCardBody>
          <CTable align="middle text-center" className="mb-0 border" hover responsive>
            <CTableBody>
              <CTableRow>
                <CTableDataCell>
                  <CImage height={300} src={ta1}></CImage>
                  <h6 className='mt-3'>LEMBAR PENGESAHAN</h6>
                </CTableDataCell>
                <CTableDataCell>
                  <CImage height={300} src={ta2}></CImage>
                  <h6 className='mt-3'>POSTER</h6>
                </CTableDataCell>
                <CTableDataCell>
                  <CImage height={300} src={ta3}></CImage>
                  <h6 className='mt-3'>LAPORAN SKRIPSI</h6>
                </CTableDataCell>
              </CTableRow>
              <CTableRow>
              <CTableDataCell colSpan={3}>
              <div>
                <CNavLink href="https://drive.google.com/file/d/13sBtMJYVvlvJBxXhCkTvrWKestOpHS9V/view?usp=sharing" target="_blank">
                  <CIcon className="me-2 text-medium-emphasis" icon={cilLink} size="xl" />
                </CNavLink>
                LAPORAN TUGAS AKHIR PDF
                <div className='small text-medium-emphasis'>
                  <span>https://drive.google.com/file/d/13sBtMJYVvlvJBxXhCkTvrWKestOpHS9V/view?usp=sharing</span>
                </div>
              </div>
              </CTableDataCell>
            </CTableRow>
            </CTableBody>
          </CTable>
        </CCardBody>
      </CCard>
    </>
  )
}

export default Ta
