import React from 'react'

import {
  CCard,
  CCardBody,
  CCardHeader,
  CImage,
  CNavLink,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableRow,
} from '@coreui/react'
import harvest from 'src/assets/images/harvestlucretia.png'
import CIcon from '@coreui/icons-react'
import { cibGithub, cibGoogleCloud, cilBrowser } from '@coreui/icons'

const Harvestlucretia = () => {
  return (
    <>
      <CCard className="mb-4">
        <CCardHeader></CCardHeader>
        <CCardBody>
          <CTable align="middle text-center" className="mb-0 border" hover responsive>
            <CTableBody>
              <CTableRow>
                <CTableDataCell>
                  <CImage height={500} src={harvest}></CImage>
                </CTableDataCell>
              </CTableRow>
              <CTableRow>
                <CTableDataCell>
                <h4>
                  Website Statis With React JS
                </h4>
                </CTableDataCell>
              </CTableRow>
              <CTableRow>
                <CTableDataCell>
                <div>
                  <CNavLink href="https://aya-harvestlucretia.vercel.app/" target="_blank">
                    <CIcon className="me-2 text-medium-emphasis" icon={cilBrowser} size="xl" />
                  </CNavLink>
                  Demo
                  <div className='small text-medium-emphasis'>
                    <span target="_blank">https://aya-harvestlucretia.vercel.app/</span>
                  </div>
                </div>
                </CTableDataCell>
              </CTableRow>
              <CTableRow>
                <CTableDataCell>
                <div>
                  <CNavLink href="https://github.com/programmercintasunnah/aya-harvestlucretia" target="_blank">
                    <CIcon className="me-2 text-medium-emphasis" icon={cibGithub} size="xl" />
                  </CNavLink>
                  Source Code
                  <div className='small text-medium-emphasis'>
                    <span target="_blank">https://github.com/programmercintasunnah/aya-harvestlucretia</span>
                  </div>
                </div>
                </CTableDataCell>
              </CTableRow>
              <CTableRow>
                <CTableDataCell>
                <div>
                  <CNavLink href="https://drive.google.com/drive/folders/1qWQysRZQyM0RBbw27ubdIuLacpmool3G?usp=sharing" target="_blank">
                    <CIcon className="me-2 text-medium-emphasis" icon={cibGoogleCloud} size="xl" />
                  </CNavLink>
                  Documentation
                  <div className='small text-medium-emphasis'>
                    <span target="_blank">https://drive.google.com/drive/folders/1qWQysRZQyM0RBbw27ubdIuLacpmool3G?usp=sharing</span>
                  </div>
                </div>
                </CTableDataCell>
              </CTableRow>
            </CTableBody>
          </CTable>
        </CCardBody>
      </CCard>
    </>
  )
}

export default Harvestlucretia
