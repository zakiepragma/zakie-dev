import React from 'react'
import CIcon from '@coreui/icons-react'
import {
  cilBuilding,
  cilClipboard,
  cilHome,
  cilSchool,
  cilSpeedometer,
  cilStar,
  cilUser,
} from '@coreui/icons'
import { CNavGroup, CNavItem, CNavTitle } from '@coreui/react'

const _nav = [
  {
    component: CNavItem,
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Profile',
    to: '/profile',
    icon: <CIcon icon={cilUser} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Certificate',
    to: '/certificate',
    icon: <CIcon icon={cilClipboard} customClassName="nav-icon" />,
  },
  {
    component: CNavTitle,
    name: 'Experience',
  },
  {
    component: CNavItem,
    name: 'Rakitek',
    to: '/rakitek',
    icon: <CIcon icon={cilHome} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Pragma',
    to: '/pragmainf',
    icon: <CIcon icon={cilBuilding} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: "Ibnu Mas'ud",
    to: '/ibnumasud',
    icon: <CIcon icon={cilSchool} customClassName="nav-icon" />,
  },
  {
    component: CNavTitle,
    name: 'Project',
  },
  {
    component: CNavItem,
    name: 'Darsalafiyah',
    to: '/darsalafiyah',
    icon: <CIcon icon={cilStar} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Harvestlucretia',
    to: '/harvestlucretia',
    icon: <CIcon icon={cilStar} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Blockchain',
    to: '/ta',
    icon: <CIcon icon={cilStar} customClassName="nav-icon" />,
  },
]

export default _nav
