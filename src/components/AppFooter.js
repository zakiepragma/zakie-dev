import React from 'react'
import { CFooter } from '@coreui/react'

const AppFooter = () => {
  return (
    <CFooter>
      <div>
        <a href="https://coreui.io" target="_blank" rel="noopener noreferrer">
          Created at
        </a>
        <span className="ms-1">&copy; June 5th, 2022 programmercintasunnah.</span>
      </div>
      <div className="ms-auto">
        <span className="me-1">Updated at</span>
        <a href="https://programmercintasunnah.com" target="_blank" rel="noopener noreferrer">
          July 13th, 2022
        </a>
      </div>
    </CFooter>
  )
}

export default React.memo(AppFooter)
