import React from 'react'

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'))
const Profile = React.lazy(() => import('./views/profile/Profile'))
const Certificate = React.lazy(() => import('./views/certificate/Certificate'))
const Rakitek = React.lazy(() => import('./views/experience/rakitek/Rakitek'))
const Pragma = React.lazy(() => import('./views/experience/pragma/Pragma'))
const IbnuMasud = React.lazy(() => import('./views/experience/ibnumasud/IbnuMasud'))
const Darsalafiyah = React.lazy(() => import('./views/project/darsalafiyah/Darsalafiyah'))
const Harvestlucretia = React.lazy(() => import('./views/project/harvestlucretia/Harvestlucretia'))
const Ta = React.lazy(() => import('./views/project/ta/Ta'))

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', element: Dashboard },
  { path: '/profile', name: 'Profile', element: Profile },
  { path: '/certificate', name: 'Certificate', element: Certificate },
  { path: '/rakitek', name: 'Rakitek', element: Rakitek },
  { path: '/pragmainf', name: 'Pragma', element: Pragma },
  { path: '/ibnumasud', name: 'IbnuMasud', element: IbnuMasud },
  { path: '/darsalafiyah', name: 'Darsalafiyah', element: Darsalafiyah },
  { path: '/harvestlucretia', name: 'Harvestlucretia', element: Harvestlucretia },
  { path: '/ta', name: 'Blockchain', element: Ta },
]

export default routes
